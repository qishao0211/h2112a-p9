#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2020/7/13 13:37
# @Author  : CoderCharm
# @File    : views.py
# @Software: PyCharm
# @Github  : github/CoderCharm
# @Email   : wg_python@163.com
# @Desc    :
"""

"""
from typing import Union, Any
from fastapi import APIRouter, Depends, Query
from sqlalchemy.orm import Session

from api.common import deps
from api.common.logger import logger
from api.utils import response_code

from .schemas import goods_schema, category_schema
from .crud.category import curd_category

router = APIRouter()


@router.get("/query/category/list", summary="查询分类列表")
async def query_category_list(
        db: Session = Depends(deps.get_db),
        token_data: Union[str, Any] = Depends(deps.check_jwt_token),
        page: int = Query(1, ge=1, title="当前页"),
        page_size: int = Query(10, le=50, title="页码长度")
):
    logger.info(f"查询分类列表->用户id:{token_data.sub}当前页{page}长度{page_size}")
    response_result = curd_category.query_all(db, page=page, page_size=page_size)
    return response_code.resp_200(data=response_result)


@router.post("/add/work", summary="工作流")
async def add_category(
        category_info: category_schema.CategoryWork,
        db: Session = Depends(deps.get_db),
):
    curd_category.create(db=db, obj_in=category_info)
    return response_code.resp_200(message="工作流添加成功")



