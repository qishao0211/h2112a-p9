from fastapi import APIRouter

from api.api_v1 import auth, goods, utils, rbacm, Company, user, work


api_v1_router = APIRouter()
api_v1_router.include_router(auth.router, prefix="/admin/auth", tags=["用户"])
api_v1_router.include_router(goods.router, prefix="/admin/goods", tags=["商品"])
api_v1_router.include_router(utils.router, prefix="/admin/utils", tags=["工具类"])
api_v1_router.include_router(rbacm.router, prefix="/admin/rbacm", tags=["角色"])
api_v1_router.include_router(Company.router, prefix="/admin/company", tags=["公司"])
api_v1_router.include_router(user.router, prefix="/admin/user", tags=["用户"])
api_v1_router.include_router(work.router, prefix="/admin/work", tags=["权限"])
