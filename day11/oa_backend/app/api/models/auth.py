from datetime import datetime

from sqlalchemy import Boolean, Column, Integer, VARCHAR, VARCHAR, BIGINT, SmallInteger, DateTime,Table,ForeignKey
from api.db.base_class import Base, gen_uuid

#


# class BaseModel(Base):
#     """ 基类模型 """
#     __abstract__ = True
#
#     # is_delete = db.Column(db.SmallInteger, default=0, comment='通过更改状态来判断记录是否被删除, 0数据有效, 1数据已删除')
#     id = db.Column(db.Integer(), primary_key=True, comment='主键，自增')
#     created_time = db.Column(db.DateTime, index=True, default=datetime.now, comment='创建时间')
#     update_time = db.Column(db.DateTime, index=True, default=datetime.now, onupdate=datetime.now, comment='修改时间')


# 角色
class Role(Base):
    __tablename__ = 'role'
    name = Column(VARCHAR(30), unique=True, comment='角色名称')
    user_id = Column(Integer, comment='用户id')
    permission_id = Column(Integer, comment='权限id')
    # users = db.relationship('User', back_populates='role')
    # permission = db.relationship('Permission', secondary=roles_permissions, back_populates='role')


# 权限
class Permission(Base):
    __tablename__ = 'permission'
    name = Column(VARCHAR(30), unique=True, comment='权限名称')
    # role_id = Column(Integer,comment='角色id')
    # role = db.relationship('Role', secondary=roles_permissions, back_populates='permission')


# 用户
class User(Base):
    __tablename__ = 'user'
    account = Column(VARCHAR(64), unique=True, comment='账号')
    password_hash = Column(VARCHAR(128), comment='密码')
    name = Column(VARCHAR(64), comment='姓名')
    status = Column(Integer, comment='状态，1为启用，2为冻结')
    role_id = Column(Integer, comment='角色id')
    # role_id = Column(db.Integer, db.ForeignKey('role.id'), comment='所属的角色id')
    # role = db.relationship('Role', back_populates='users')


# 项目
class Project(Base):
    __tablename__ = 'project'
    user_id = Column(Integer, comment='所属的用户id')
    num = Column(Integer, comment='编号')
    name = Column(VARCHAR(64), comment='项目名称')
    environment_choice = Column(VARCHAR(16), comment='环境选择，first为测试，以此类推')
    environment_list = Column(VARCHAR(1024), comment='环境列表')
    environment = Column(VARCHAR(1024), comment='环境')
    principal = Column(VARCHAR(16))
    apiset_id = Column(Integer, comment='接口id')
    config_id = Column(Integer, comment='配置id')
    caseset_id = Column(Integer, comment='用例id')


# 接口
class ApiSet(Base):
    __tablename__ = 'api_set'
    name = Column(VARCHAR(64), nullable=True, comment='接口模块')
    num = Column(Integer, nullable=True, comment='模块序号')
    project_id = Column(Integer, comment='所属项目id')
    apimsg = Column(Integer,comment='接口详情')



# 接口详情表
class ApiMsg(Base):
    __tablename__ = 'api_msg'
    num = Column(Integer, nullable=True, comment='接口序号')
    name = Column(VARCHAR(128), nullable=True, comment='接口名称')
    desc = Column(VARCHAR(256), nullable=True, comment='接口描述')
    variable_type = Column(VARCHAR(32), nullable=True, comment='参数类型选择')
    status_url = Column(VARCHAR(32), nullable=True, comment='基础url,序号对应项目的环境')
    up_func = Column(VARCHAR(128), comment='接口执行前的函数')
    down_func = Column(VARCHAR(128), comment='接口执行后的函数')
    method = Column(VARCHAR(32), nullable=True, comment='请求方式')
    variable = Column(VARCHAR(2048), comment='form-data形式的参数')
    json_variable = Column(VARCHAR(2048), comment='json形式的参数')
    swagger_json_variable = Column(VARCHAR(2048), comment='swagger_json形式的参数')
    param = Column(VARCHAR(2048), comment='url上面所带的参数')
    url = Column(VARCHAR(256), nullable=True, comment='接口地址')
    validate = Column(VARCHAR(2048), comment='断言信息')
    header = Column(VARCHAR(2048), comment='头部信息')
    apiset_id = Column(Integer,comment='接口模型id')
    project_id = Column(Integer,comment='项目id')



# 用例
class CaseSet(Base):
    __tablename__ = 'case_set'
    num = Column(Integer, nullable=True, comment='用例集合序号')
    name = Column(VARCHAR(256), nullable=True, comment='用例集名称')
    project_id = Column(Integer,comment='项目id')



class CaseData(Base):
    __tablename__ = 'case_data'
    num = Column(Integer, nullable=True, comment='步骤序号，执行顺序按序号来')
    status = Column(VARCHAR(16), comment='状态，true表示执行，false表示不执行')
    name = Column(VARCHAR(128), comment='步骤名称')
    up_func = Column(VARCHAR(256), comment='步骤执行前的函数')
    down_func = Column(VARCHAR(256), comment='步骤执行后的函数')
    skip = Column(VARCHAR(64), comment='跳过判断函数')
    time = Column(Integer, default=1, comment='执行次数')
    param = Column(VARCHAR(2048), default=u'[]')
    status_param = Column(VARCHAR(64), default=u'[true, true]')
    variable = Column(VARCHAR(2048))
    json_variable = Column(VARCHAR(2048))
    status_variables = Column(VARCHAR(64))
    extract = Column(VARCHAR(2048))
    status_extract = Column(VARCHAR(64))
    validate = Column(VARCHAR(2048))
    status_validate = Column(VARCHAR(64))
    header = Column(VARCHAR(2048))
    status_header = Column(VARCHAR(64))
    parameters = Column(VARCHAR(2048))
    status_parameters = Column(Boolean)
    apimsg_id = Column(Integer,comment='接口详情id')
    # api_msg_id = db.Column(db.Integer, db.ForeignKey('api_msg.id'))
    # __mapper_args__ = {"order_by": num.asc()}


class Report(Base):
    __tablename__ = 'report'
    id = Column(Integer, primary_key=True, comment='主键，自增')
    case_names = Column(VARCHAR(2048), nullable=True, comment='用例的名称集合')
    read_status = Column(VARCHAR(16), nullable=True, comment='阅读状态')
    performer = Column(VARCHAR(16), nullable=True, comment='执行者')
    project_id = Column(VARCHAR(16), nullable=True)
    result = Column(VARCHAR(16), comment='结果')
    create_time = Column(DateTime(), index=True, default=datetime.now)
    # create_time = db.Column(db.DateTime, index=True, default=datetime.now, comment='创建时间')


class Task(Base):
    __tablename__ = 'tasks'
    num = Column(Integer, comment='任务序号')
    task_name = Column(VARCHAR(64), comment='任务名称')
    task_config_time = Column(VARCHAR(256), nullable=True, comment='cron表达式')
    set_id = Column(VARCHAR(2048))
    case_id = Column(VARCHAR(2048))
    task_type = Column(VARCHAR(16))
    task_to_email_address = Column(VARCHAR(256), comment='收件人邮箱')
    task_send_email_address = Column(VARCHAR(256), comment='发件人邮箱')
    webhook = Column(VARCHAR(256), comment='钉钉机器人webhook字段')
    secret = Column(VARCHAR(256), comment='钉钉机器人secret字段')
    title = Column(VARCHAR(256), comment='钉钉标题')
    email_password = Column(VARCHAR(256), comment='发件人邮箱密码')
    status = Column(VARCHAR(16), default=u'创建', comment='任务的运行状态，默认是创建')
    project_id = Column(VARCHAR(16), nullable=True)
    send_email_status = Column(VARCHAR(16))



class Logs(Base):
    __tablename__ = 'logs'
    ip = Column(VARCHAR(128), comment='ip')
    uid = Column(VARCHAR(128), comment='uid')
    url = Column(VARCHAR(128), comment='url')


class RolesPermissions(Base):
    __tablename__ = 'roles_permissions'
    role_id = Column(Integer,ForeignKey('role.id')),
    permission_id = Column(Integer,ForeignKey('permission.id'))

class UserProject(Base):
    __tablename__ = 'user_project'
    user_id = Column(Integer,ForeignKey('user.id')),
    project_id = Column(Integer,ForeignKey('project.id'))

