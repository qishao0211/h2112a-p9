#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2020/7/13 13:37
# @Author  : CoderCharm
# @File    : views.py
# @Software: PyCharm
# @Github  : github/CoderCharm
# @Email   : wg_python@163.com
# @Desc    :
"""

"""
from typing import Union, Any
from fastapi import APIRouter, Depends, Query
from sqlalchemy.orm import Session

from api.common import deps
from api.common.logger import logger
from api.utils import response_code

from .schemas import goods_schema, category_schema
from .crud.category import curd_category

router = APIRouter()

from fastapi.responses import JSONResponse
@router.api_route('/ss',methods=['GET'])
async def index():
    return JSONResponse({'code':200})

@router.post("/add/role", summary="添加角色")
async def add_category(
        category_info: category_schema.CategoryCreate,
        db: Session = Depends(deps.get_db),
):
    logger.info(f"添加分类->用户id:，角色名:{category_info.name}")
    curd_category.create(db=db, obj_in=category_info)
    return response_code.resp_200(message="角色添加成功")


@router.get("/get/role", summary="查询角色列表")
async def query_category_list(
        db: Session = Depends(deps.get_db),
        page: int = Query(1, ge=1, title="当前页"),
        page_size: int = Query(10, le=50, title="页码长度")
):
    logger.info(f"查询分类列表->用户id:当前页{page}长度{page_size}")
    response_result = curd_category.query_all(db, page=page, page_size=page_size)
    return response_code.resp_200(data=response_result)

@router.post("/add/reso", summary="添加资源")
async def add_category(
        category_info: category_schema.CategoryCreate2,
        db: Session = Depends(deps.get_db),
):
    logger.info(f"添加分类->用户id:，资源名:{category_info.rname}")
    curd_category.create2(db=db, obj_in=category_info)
    return response_code.resp_200(message="资源添加成功")

@router.get("/get/reso", summary="查询角色列表")
async def query_category_list(
        db: Session = Depends(deps.get_db),
        page: int = Query(1, ge=1, title="当前页"),
        page_size: int = Query(10, le=50, title="页码长度")
):
    logger.info(f"查询分类列表->用户id:当前页{page}长度{page_size}")
    response_result = curd_category.query_all2(db, page=page, page_size=page_size)
    return response_code.resp_200(data=response_result)

@router.post("/add/rr", summary="添加资源")
async def add_category(
        category_info: category_schema.CategoryCreate3,
        db: Session = Depends(deps.get_db),
):
    logger.info(f"添加分类->用户id:，资源名:{category_info.role_id}")
    curd_category.create3(db=db, obj_in=category_info)
    return response_code.resp_200(message="资源添加成功")

@router.get("/get/rr", summary="查询角色列表")
async def query_category_list(
        db: Session = Depends(deps.get_db),
        page: int = Query(1, ge=1, title="当前页"),
        page_size: int = Query(10, le=50, title="页码长度")
):
    logger.info(f"查询分类列表->用户id:当前页{page}长度{page_size}")
    response_result = curd_category.query_all3(db, page=page, page_size=page_size)
    return response_code.resp_200(data=response_result)
