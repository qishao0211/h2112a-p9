import redis

from core.config.development_config import settings

class MyRedis():
    def __init__(self):
        pool = redis.ConnectionPool()
        self.r = redis.Redis(connection_pool=pool, host=settings.REDIS_HOST, port=settings.REDIS_PORT)

    def set_str(self, key, value, time=0):
        if not time:
            self.r.set(key, value)
        else:
            self.r.set(key, value, ex=time)

    def get_str(self, key):
        res = self.r.get(key)
        if res:
            return res.decode()
        else:
            return False


myredis = MyRedis()
myredis.set_str('sss1', 2, time=10)
# print(myredis.get_str('sss'))