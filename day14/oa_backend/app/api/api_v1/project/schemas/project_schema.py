#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2020/7/11 10:57
# @Author  : CoderCharm
# @File    : project_schema.py
# @Software: PyCharm
# @Desc    :
"""
权限表
"""
from typing import Optional

from pydantic import BaseModel


class ProjectCreate(BaseModel):
    """
    创建项目字段
    """
    user_id: int = 1
    name: str
    environment_list: str
    user_name: str


class RoleUpdate(BaseModel):
    """
    角色更新字段
    """
    role_name: Optional[str] = None
    re_mark: Optional[str] = None
