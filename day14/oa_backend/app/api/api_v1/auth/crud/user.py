# #!/usr/bin/env python
# # -*- coding: utf-8 -*-
# # @Time    : 2020/7/7 16:38
# # @Author  : CoderCharm
# # @File    : curd_user.py
# # @Software: PyCharm
# # @Desc    :
# """
#
# """
#
from typing import Optional
import re
from api.utils.myredis import myredis

from sqlalchemy.orm import Session
from api.utils.myjwt import myjwt
from core.security import get_password_hash, verify_password
from api.common.curd_base import CRUDBase
from api.models.auth import User
from ..schemas import user_schema


class CRUDUser(CRUDBase[User, user_schema.UserCreate, user_schema.UserUpdate]):

    @staticmethod
    def get_by_email(db: Session, *, email: str) -> Optional[User]:
        """
        通过email获取用户
        参数里面的* 表示 后面调用的时候 要用指定参数的方法调用
        正确调用方式
            curd_user.get_by_email(db, email="xxx")
        错误调用方式
            curd_user.get_by_email(db, "xxx")
        :param db:
        :param email:
        :return:
        """
        return db.query(User).filter(User.email == email).first()

    def create(self, db: Session, *, obj_in: user_schema.UserCreate) -> User:
        db_obj = User(
            nickname=obj_in.nickname,
            email=obj_in.email,
            hashed_password=get_password_hash(obj_in.password),
            avatar=obj_in.avatar,
            role_id=obj_in.role_id,
            is_active=obj_in.is_active
        )
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    def authenticate(self, db: Session, *, email: str, password: str) -> Optional[User]:
        user = self.get_by_email(db, email=email)
        if not user:
            return None
        if not verify_password(password, user.hashed_password):
            return None
        return user

    @staticmethod
    def is_active(user: User) -> bool:
        return user.is_active == 1

    # 判断手机号格式
    @staticmethod
    def mobile_auth(mobile) -> bool:
        res = re.findall(r'^1[3-9][0-9]{9}$', mobile)
        if res:
            return True
        return False

    # 判断手机号
    @staticmethod
    def mobile_info(db, mobile) -> bool:
        user_inof = db.query(User).filter(User.account == mobile).first()
        if user_inof:
            return True
        return False

    # 判断验证码
    @staticmethod
    def mobile_code(mobile) -> str:
        res = myredis.get_str(mobile)
        if res:
            return res
        return res

    # 判断验证码
    @staticmethod
    def user_token(db, mobile) -> str:
        user_inof = db.query(User).filter(User.account == mobile).first()
        payload = {
            'id': user_inof.id,
            'mobile': mobile
        }
        token = myjwt.jwt_token(payload)
        return token


curd_user = CRUDUser(User)
