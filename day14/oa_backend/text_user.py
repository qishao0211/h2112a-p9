import unittest
import requests
import HTMLTestRunner


class TestLogin(unittest.TestCase):
    def setUp(self) -> None:
        print('开始')

    def login_user_password(self):
        url = 'http://127.0.0.1:8010/api/mall/v1/admin/auth/login?mobile=3111111112&code=222'
        # 解析返回参数
        res = requests.post(url).json()
        # 对比
        recode = 200
        self.assertEquals(recode, res['code'])

    def username_valid(self):
        url = 'http://127.0.0.1:8010/api/mall/v1/admin/auth/login?mobile=13111111112&code=222'
        # 解析返回参数
        res = requests.post(url).json()
        # 对比
        recode = 200
        self.assertEquals(recode, res['code'])

    def tearDown(self) -> None:
        print('结束')
        print('='*30)


if __name__ == '__main__':
    # 创建测试集
    suit = unittest.TestSuite()
    suit.addTests([TestLogin('login_user_password'), TestLogin('username_valid')])

    # 启动
    # runner = unittest.TextTestRunner()
    # runner.run(suit)

    with open('reponse.html', 'wb') as f:
        runer = HTMLTestRunner.HTMLTestRunner(stream=f)
        runer.run(suit)
