import redis


class MyRedis():
    def __init__(self):
        pool = redis.ConnectionPool()
        self.r = redis.Redis(connection_pool=pool)

    def set_str(self, key, value):
        self.r.set(key, value)

    def get_str(self, key):
        res = self.r.get(key)
        if not res:
            return False
        return res.decode()


myredis = MyRedis()