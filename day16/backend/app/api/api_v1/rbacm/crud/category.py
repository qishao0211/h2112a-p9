#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2020/7/25 09:02
# @Author  : CoderCharm
# @File    : category.py
# @Software: PyCharm
# @Github  : github/CoderCharm
# @Email   : wg_python@163.com
# @Desc    :
"""

"""
import redis
from pydantic import conint
from sqlalchemy import func, or_
from sqlalchemy.orm import Session

from api.common.curd_base import CRUDBase
from api.models.goods import MallCategory
from api.models.auth import Role, Resource, RoleResource, RoleResource
from ..schemas import role_schema
from api.redis.myredis import myredis


class CRUDCategory(CRUDBase[MallCategory, role_schema.CategoryCreate, role_schema.CategoryUpdate]):

    def query_obj(self, db: Session, *, cate_id: int) -> dict:
        """
        查询单条数据
        :param db:
        :param cate_id:
        :return:
        """
        obj = self.get(db=db, id=cate_id)
        if not obj:
            return {}
        return {"id": obj.id, 'create_time': obj.create_time.strftime('%Y-%m-%d %H:%M:%S'), "name": obj.name,
                "front_desc": obj.front_desc, "parent_id": obj.parent_id, "sort_order": obj.sort_order,
                "icon_url": obj.icon_url, "enabled": obj.enabled}

    @staticmethod
    def search_field(db: Session, *, cate_info: role_schema.CategorySearch):
        temp_page = (cate_info.page - 1) * cate_info.page_size
        # 查询数量包含关键词的数量
        total = db.query(func.count(MallCategory.id)).filter(
            or_(MallCategory.name.contains(cate_info.key_world),
                MallCategory.front_desc.contains(cate_info.key_world))).scalar()
        # 查询name和front_desc包含搜索关键词的数据并分页
        search_obj = db.query(MallCategory).filter(
            or_(MallCategory.name.contains(cate_info.key_world),
                MallCategory.front_desc.contains(cate_info.key_world))).offset(
            temp_page).limit(cate_info.page_size).all()

        items = [{"id": obj.id, 'create_time': obj.create_time.strftime('%Y-%m-%d %H:%M:%S'), "name": obj.name,
                  "front_desc": obj.front_desc, "sort_order": obj.sort_order,
                  "icon_url": obj.icon_url, "enabled": obj.enabled} for obj in search_obj]
        return {
            "items": items,
            "total": total
        }

    @staticmethod
    def query_all(db: Session, *, page: int = 1, page_size: conint(le=50) = 10) -> dict:
        """
        查询数据列表
        :param db:
        :param page:
        :param page_size:
        :return:
        """
        temp_page = (page - 1) * page_size
        # 查询数量
        total = db.query(func.count(Role.id)).filter(Role.pid != 0).scalar()
        # 查询结果集
        query_obj = db.query(Role).filter(Role.pid != 0).offset(
            temp_page).limit(page_size).all()

        # 获取一级分类
        data = db.query(Role).filter(Role.pid == 0).all()
        one_list = [{'value': i.id, 'label': i.rname} for i in data]

        items = [{"id": obj.id, 'create_time': obj.create_time.strftime('%Y-%m-%d %H:%M:%S'), "rname": obj.rname} for
                 obj in query_obj]

        return {
            "items": items,
            "total": total,
            'data': one_list,

        }


    def create(self, db: Session, *, obj_in: role_schema.RoleCreate) -> Role:
        db_obj = Role(
            rname=obj_in.name,
            pid=obj_in.pid,
            roletype=obj_in.roletype
        )
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    @staticmethod
    def update_cate(db: Session, *, obj_in: role_schema.CategoryUpdate):
        db.query(MallCategory).filter(MallCategory.id == obj_in.id).update({
            MallCategory.name: obj_in.name,
            MallCategory.front_desc: obj_in.front_desc,
            MallCategory.sort_order: obj_in.sort_order,
            MallCategory.icon_url: obj_in.icon_url,
            MallCategory.enabled: obj_in.enabled
        })
        db.commit()

    @staticmethod
    def update_enabled(db: Session, *, id: int, enabled: int):
        db.query(MallCategory).filter(MallCategory.id == id).update({MallCategory.enabled: enabled})
        db.commit()


curd_category = CRUDCategory(Role)


# 根式或数据
def data_get(data):
    if not data:
        return []
    # 把接收到的数据重组
    list_id = []
    list_res = []
    for i in data:
        if i['pid'] not in list_id:
            list_res.append({"id": i['pid'], "name": i['pname'], "son": [{"id": i['id'],'url': i['url'], 'name': i['rname']}]})
            list_id.append(i['pid'])
        else:
            index = list_id.index(i['pid'])
            list_res[index]['son'].append({"id": i['id'],'url': i['url'], 'name': i['rname']})
    return list_res


class CRUDresource(CRUDBase[MallCategory, role_schema.CategoryCreate, role_schema.CategoryUpdate]):

    def query_obj(self, db: Session, *, cate_id: int) -> dict:
        """
        查询单条数据
        :param db:
        :param cate_id:
        :return:
        """
        obj = self.get(db=db, id=cate_id)

        if not obj:
            return {}
        return {"id": obj.id, 'create_time': obj.create_time.strftime('%Y-%m-%d %H:%M:%S'), "name": obj.name,
                "front_desc": obj.front_desc, "parent_id": obj.parent_id, "sort_order": obj.sort_order,
                "icon_url": obj.icon_url, "enabled": obj.enabled}

    @staticmethod
    def search_field(db: Session, *, cate_info: role_schema.CategorySearch):
        temp_page = (cate_info.page - 1) * cate_info.page_size
        # 查询数量包含关键词的数量
        total = db.query(func.count(MallCategory.id)).filter(
            or_(MallCategory.name.contains(cate_info.key_world),
                MallCategory.front_desc.contains(cate_info.key_world))).scalar()
        # 查询name和front_desc包含搜索关键词的数据并分页
        search_obj = db.query(MallCategory).filter(
            or_(MallCategory.name.contains(cate_info.key_world),
                MallCategory.front_desc.contains(cate_info.key_world))).offset(
            temp_page).limit(cate_info.page_size).all()

        items = [{"id": obj.id, 'create_time': obj.create_time.strftime('%Y-%m-%d %H:%M:%S'), "name": obj.name,
                  "front_desc": obj.front_desc, "sort_order": obj.sort_order,
                  "icon_url": obj.icon_url, "enabled": obj.enabled} for obj in search_obj]
        return {
            "items": items,
            "total": total
        }

    @staticmethod
    def query_all(db: Session, *, page: int = 1, page_size: conint(le=50) = 10) -> dict:
        """
        查询数据列表
        :param db:
        :param page:
        :param page_size:
        :return:
        """
        temp_page = (page - 1) * page_size
        # 查询数量
        total = db.query(func.count(Resource.id)).scalar()
        # 查询结果集
        query_obj = db.query(Resource).filter(Resource.is_delete == 0).offset(
            temp_page).limit(page_size).all()

        items = [{"id": obj.id, 'create_time': obj.create_time.strftime('%Y-%m-%d %H:%M:%S'), "rname": obj.rname,
                  'pid': obj.pid, 'url': obj.url, 'type': obj.type} for
                 obj in query_obj]
        return {
            "items": items,
            "total": total
        }

    @staticmethod
    def get_all(db: Session) -> dict:
        # 查询结果集
        query_obj = db.query(Resource).filter(Resource.pid != 0).all()

        items = [{"key": obj.id, "label": obj.rname,
                  'disabled': 0} for
                 obj in query_obj]
        return {
            "items": items,
        }

    @staticmethod
    def role_all(db: Session, role_id: int) -> dict:
        # 查询结果集
        resource_obj = db.query(RoleResource).filter(RoleResource.role_id == role_id).all()

        if not resource_obj:
            return {'items': []}
        items = [obj.resource_id for obj in resource_obj]
        return {
            "items": items,
        }

    @staticmethod
    def user_resource_all(db: Session, role_id: int) -> list:
        # 查询结果集
        sql = f"select res.rname,res.url,res.pid,par.id,par.rname as pname from role_resource as rr left join resource as res on rr.resource_id=res.id left join resource as par on res.pid=par.id where role_id={role_id}"
        data = db.execute(sql)

        # 序列化数据
        datas = data_get(data)
        return datas

        # if not resource_obj:
        #     return {'items': []}
        # items = [obj.resource_id for obj in resource_obj]
        # return {
        #     "items": items,
        # }

    # @staticmethod
    # def role_resource_add(db: Session, resource_info) -> dict:
    #     # 删除对应的数据
    #     db.query(RoleResource).filter(RoleResource.role_id == resource_info.role_id).delete()
    #
    #     resource_id_list = resource_info.resource_list.split(',')
    #
    #     if not resource_info.resource_list:
    #         db.commit()
    #         return True
    #
    #     # 解决互斥
    #     for i in resource_id_list:
    #         sql = f"select * from mutual where resource_id1={int(i)} or resource_id2={int(i)}"
    #         res = db.execute(sql)
    #         res_list = []
    #         for rid in res:
    #             res_list.append(str(rid.resource_id1))
    #             res_list.append(str(rid.resource_id2))
    #         # 合并&去重
    #         inter_list = list(set(resource_id_list).intersection(set(res_list)))
    #         print('inter_list: ', inter_list)
    #         if len(inter_list) > 1:
    #             print('>>>>>++++++++')
    #             return False
    #
    #     # 配置资源
    #     db_obj = []
    #     for i in resource_id_list:
    #         db_obj = RoleResource(
    #             role_id=resource_info.role_id,
    #             resource_id=i
    #         )
    #         db.add(db_obj)
    #         db.commit()
    #         db.refresh(db_obj)
    #     return db_obj

    @staticmethod
    def role_resource_add(db: Session, resource_info) -> dict:
        # 删除对应的数据
        db.query(RoleResource).filter(RoleResource.role_id == resource_info.role_id).delete()
        resource_id_list = resource_info.resource_list.split(',')

        if not resource_info.resource_list:
            db.commit()
            return True

        # 解决互斥
        promition = 0
        for i in resource_id_list:
            resource = db.query(Resource).filter(Resource.id == i).first()
            if resource.promition:
                promition = promition | resource.promition

        roles = db.query(Role).filter(Role.id == resource_info.role_id).first()
        if roles.pid > 0:
            parent = db.query(Role).filter(Role.id == roles.pid).first()
            print('role:', parent.promition)
            print('=====', promition)
            promition2 = promition | parent.promition
            if promition == promition2 or parent.promition == promition2:
                print('==已发生互斥==')
                return False
            print('promition2', promition2)
        print('+++++++++')
        sql = f"update role set promition={promition2} where id={resource_info.role_id}"
        db.execute(sql)
        db.commit()

        # 配置资源
        db_obj = []
        for i in resource_id_list:
            db_obj = RoleResource(
                role_id=resource_info.role_id,
                resource_id=i)
            db.add(db_obj)
            db.commit()
            db.refresh(db_obj)
        return db_obj

    def create(self, db: Session, *, obj_in: role_schema.ResourceCreate) -> Resource:
        # 获取资源的二进制编号
        key = 'resource_1'
        if not obj_in.pid:
            num = 0
        else:
            num = int(myredis.get_str(key)) * 2

        db_obj = Resource(
            rname=obj_in.rname,
            pid=obj_in.pid,
            url=obj_in.url,
            type=obj_in.type,
            promition=num
        )
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)

        # 更新reids二进制编号
        if num:
            myredis.set_str(key, num)
        return db_obj

    @staticmethod
    def update_cate(db: Session, *, obj_in: role_schema.CategoryUpdate):
        db.query(MallCategory).filter(MallCategory.id == obj_in.id).update({
            MallCategory.name: obj_in.name,
            MallCategory.front_desc: obj_in.front_desc,
            MallCategory.sort_order: obj_in.sort_order,
            MallCategory.icon_url: obj_in.icon_url,
            MallCategory.enabled: obj_in.enabled
        })
        db.commit()

    @staticmethod
    def update_enabled(db: Session, *, id: int, enabled: int):
        db.query(MallCategory).filter(MallCategory.id == id).update({MallCategory.enabled: enabled})
        db.commit()


curd_resource = CRUDresource(Resource)


class CRUDresources(CRUDBase[MallCategory, role_schema.CategoryCreate, role_schema.CategoryUpdate]):

    def query_obj(self, db: Session, *, cate_id: int) -> dict:
        """
        查询单条数据
        :param db:
        :param cate_id:
        :return:
        """
        obj = self.get(db=db, id=cate_id)
        if not obj:
            return {}
        return {"id": obj.id, 'create_time': obj.create_time.strftime('%Y-%m-%d %H:%M:%S'), "name": obj.name,
                "front_desc": obj.front_desc, "parent_id": obj.parent_id, "sort_order": obj.sort_order,
                "icon_url": obj.icon_url, "enabled": obj.enabled}

    @staticmethod
    def search_field(db: Session, *, cate_info: role_schema.CategorySearch):
        temp_page = (cate_info.page - 1) * cate_info.page_size
        # 查询数量包含关键词的数量
        total = db.query(func.count(MallCategory.id)).filter(
            or_(MallCategory.name.contains(cate_info.key_world),
                MallCategory.front_desc.contains(cate_info.key_world))).scalar()
        # 查询name和front_desc包含搜索关键词的数据并分页
        search_obj = db.query(MallCategory).filter(
            or_(MallCategory.name.contains(cate_info.key_world),
                MallCategory.front_desc.contains(cate_info.key_world))).offset(
            temp_page).limit(cate_info.page_size).all()

        items = [{"id": obj.id, 'create_time': obj.create_time.strftime('%Y-%m-%d %H:%M:%S'), "name": obj.name,
                  "front_desc": obj.front_desc, "sort_order": obj.sort_order,
                  "icon_url": obj.icon_url, "enabled": obj.enabled} for obj in search_obj]
        return {
            "items": items,
            "total": total
        }

    @staticmethod
    def query_all(db: Session, *, page: int = 1, page_size: conint(le=50) = 10) -> dict:
        """
        查询数据列表
        :param db:
        :param page:
        :param page_size:
        :return:
        """
        temp_page = (page - 1) * page_size
        # 查询数量
        total = db.query(func.count(Resource.id)).scalar()
        # 查询结果集
        query_obj = db.query(Resource).filter(Resource.is_delete == 0).offset(
            temp_page).limit(page_size).all()

        items = [{"id": obj.id, 'create_time': obj.create_time.strftime('%Y-%m-%d %H:%M:%S'), "rname": obj.rname,
                  'pid': obj.pid, 'url': obj.url, 'type': obj.type} for
                 obj in query_obj]
        return {
            "items": items,
            "total": total
        }

    def create(self, db: Session, *, obj_in: role_schema.RoleResourceCreate) -> RoleResource:
        db_obj = RoleResource(
            role_id=obj_in.role_id,
            resource_id=obj_in.resource_id
        )
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    @staticmethod
    def update_cate(db: Session, *, obj_in: role_schema.CategoryUpdate):
        db.query(MallCategory).filter(MallCategory.id == obj_in.id).update({
            MallCategory.name: obj_in.name,
            MallCategory.front_desc: obj_in.front_desc,
            MallCategory.sort_order: obj_in.sort_order,
            MallCategory.icon_url: obj_in.icon_url,
            MallCategory.enabled: obj_in.enabled
        })
        db.commit()

    @staticmethod
    def update_enabled(db: Session, *, id: int, enabled: int):
        db.query(MallCategory).filter(MallCategory.id == id).update({MallCategory.enabled: enabled})
        db.commit()


curd_role_resource = CRUDresources(Resource)
