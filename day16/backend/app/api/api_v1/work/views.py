#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2020/7/13 13:37
# @Author  : CoderCharm
# @File    : views.py
# @Software: PyCharm
# @Github  : github/CoderCharm
# @Email   : wg_python@163.com
# @Desc    :
"""

"""
from typing import Union, Any
from fastapi import APIRouter, Depends, Query
from sqlalchemy.orm import Session

from api.common import deps
from api.common.logger import logger
from api.utils import response_code

from .schemas import goods_schema, category_schema
from .crud.category import curd_category

router = APIRouter()


@router.get("/get/work", summary="查询工作流")
async def query_category_list(
        db: Session = Depends(deps.get_db),
):
    response_result = curd_category.query_all(db)
    return response_code.resp_200(data=response_result)


@router.post("/add/work", summary="工作流")
async def add_category(
        category_info: category_schema.CategoryWork,
        db: Session = Depends(deps.get_db),
):
    curd_category.create(db=db, obj_in=category_info)
    return response_code.resp_200(message="工作流添加成功")


@router.post("/put/work", summary="给工作流设置属性")
async def put_category(
        category_info: category_schema.CategoryWorkUpd,
        db: Session = Depends(deps.get_db),
):
    curd_category.update_work(db=db, obj_in=category_info)
    return response_code.resp_200(message="设置成功")


@router.post("/add/work/audit", summary="添加审核流程")
async def put_category(
        category_info: category_schema.CategoryWorkAudit,
        db: Session = Depends(deps.get_db),
):
    curd_category.work_audit(db=db, obj_in=category_info)
    return response_code.resp_200(message="设置成功")
