#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2020/7/7 14:11
# @Author  : CoderCharm
# @File    : views.py
# @Software: PyCharm
# @Desc    :
"""

"""
from datetime import timedelta
from typing import Any, Union
from fastapi import Body, requests
from sqlalchemy.orm import Session
from fastapi import APIRouter, Depends
from core import security

from api.common import deps
from api.utils import response_code
from api.common.logger import logger

from api.models import auth
from core.config import settings

from .schemas import user_schema, token_schema

from .crud import curd_user, curd_role

router = APIRouter()


@router.post("/login/access-token", summary="用户登录认证", response_model=token_schema.RespToken)
async def login_access_token(
        *,
        db: Session = Depends(deps.get_db),
        user_info: user_schema.UserEmailAuth,
) -> Any:
    """
    用户登录
    :param db:
    :param user_info:
    :return:
    """

    # 验证用户
    user = curd_user.authenticate(db, email=user_info.username, password=user_info.password)
    if not user:
        logger.info(f"用户邮箱认证错误: email{user_info.username} password:{user_info.password}")
        return response_code.resp_500(message="用户名或者密码错误")
    elif not curd_user.is_active(user):
        return response_code.resp_500(message="用户邮箱未激活")

    access_token_expires = timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)

    # 登录token 只存放了user.id
    return response_code.resp_200(data={
        "token": security.create_access_token(user.id, expires_delta=access_token_expires),
    })


@router.get("/user/info", summary="获取用户信息", response_model=user_schema.RespUserInfo)
async def get_user_info(
        *,
        db: Session = Depends(deps.get_db),
        current_user: auth.AdminUser = Depends(deps.get_current_user)
) -> Any:
    """
    获取用户信息
    :param db:
    :param current_user:
    :return:
    """
    role_info = curd_role.query_role(db, role_id=current_user.role_id)

    return response_code.resp_200(data={
        "role_id": current_user.role_id,
        "rbacm": role_info.role_name,
        "nickname": current_user.nickname,
        "avatar": current_user.avatar
    })


@router.post("/user/logout", summary="用户退出", response_model=user_schema.RespBase)
async def user_logout(token_data: Union[str, Any] = Depends(deps.check_jwt_token),):
    """
    用户退出
    :param token_data:
    :return:
    """
    logger.info(f"用户退出->用户id:{token_data.sub}")
    return response_code.resp_200(message="logout success")


@router.post("/user/login", summary="用户登录")
async def login_access_token(
        user_info: user_schema.UserLogin,
        db: Session = Depends(deps.get_db),
):
    # 验证用户
    user = curd_user.userauthenticate(db, account=user_info.account, password=user_info.password)
    if not user:
        # logger.info(f"用户邮箱认证错误: email{user_info.username} password:{user_info.password}")
        return response_code.resp_500(message="用户名或者密码错误")

    # 登录token 只存放了user.id
    return response_code.resp_200(data={
        "token": security.token_encode(user),
    })


"""
uname = Column(VARCHAR(25), comment="姓名")
account = Column(VARCHAR(25), comment="登录账号")
number = Column(VARCHAR(30), comment="工号")
mobile = Column(VARCHAR(11), comment="手机号")
password = Column(VARCHAR(32), comment="密码")
company_id = Column(Integer, comment="所属公司")
station_id = Column(Integer, comment="岗位id")
dutuies_id = Column(Integer, comment="职务id")
role_id = Column(Integer, comment="角色id")
"""