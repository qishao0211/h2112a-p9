from app import app
from flask_cors import CORS
from comm.config import Config
from views.colle import coll_dp

app.config.from_object(Config)
CORS(app)
app.register_blueprint(coll_dp)
if __name__ == '__main__':
    app.run()
