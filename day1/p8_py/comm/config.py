from pickle import TRUE

def Config():
    DEBUG = TRUE
    JSON_AS_ASCII = False
    BROKER_URL = ''
    CELERY_RESULT_BACKEND = ''
