import pymysql
class Db():
    def __init__(self) -> None:
        self.conn = pymysql.connect(host='localhost',port=3306,user='root',
        password='shayebushi',db='p9_logis',cursorclass=pymysql.cursors.DictCursor)
        self.cursor = self.conn.cursor()

    def find(self,sql):     #查询单个数据
        self.cursor.execute(sql)
        res = self.cursor.fetchone()
        return res


    def find_all(self,sql):   #查询全部数据
        self.cursor.execute(sql)
        res = self.cursor.fetchall()
        return res

    def update(self,sql):   #修改数据
        self.cursor.execute(sql)
        return self.cursor.lastrowid

    def commit(self):   #保存
        self.conn.commit()


    def rollback(self):   #回滚
        self.conn.rollback()



db = Db()
