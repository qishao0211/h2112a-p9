from flask import Blueprint,jsonify
from flask_restful import reqparse
import random,datetime

from db import db

coll_dp=Blueprint('coll_dp',__name__)

@coll_dp.route('/coll')
def collection_view():   #获取全部数据
    sql='select * from collection'
    coll_info=db.find_all(sql)
    coll_data=[str(i) for i in coll_info]
    print(coll_info)
    return jsonify({'code':200,'msg':'收款数据获取成功','data':coll_data})

@coll_dp.route('/coll2',methods=['GET','POST'])
def collection_view2():
    req=reqparse.RequestParser()
    req.add_argument('payment')  #付款金额
    req.add_argument('order_id')  #订单编号
    req.add_argument('user_id')  #收款人
    args=req.parse_args()
    ss = datetime.datetime.now()
    date = str(ss).split('.')[0]    #订单日期
    sql="insert into `collection` (`order_id`, `date`, `payment`, `user_id`) " \
        f"values('{args['order_id']}','{date}','{args['payment']}','{args['user_id']}');"
    db.update(sql)
    db.commit()

    return jsonify({"code":200,'msg':'添加成功'})
