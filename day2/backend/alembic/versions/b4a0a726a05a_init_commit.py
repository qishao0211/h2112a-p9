"""init commit

Revision ID: b4a0a726a05a
Revises: be46c64a47f7
Create Date: 2022-09-14 19:08:39.493931

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'b4a0a726a05a'
down_revision = 'be46c64a47f7'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('audit',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('create_time', sa.DateTime(), server_default=sa.text('now()'), nullable=True, comment='创建时间'),
    sa.Column('update_time', sa.DateTime(), server_default=sa.text('now()'), nullable=True, comment='更新时间'),
    sa.Column('is_delete', sa.Integer(), server_default='0', nullable=True, comment='逻辑删除:0=未删除,1=删除'),
    sa.Column('work_id', sa.Integer(), nullable=True, comment='工作流id'),
    sa.Column('task_id', sa.Integer(), nullable=True, comment='任务id'),
    sa.Column('audit_user', sa.Integer(), nullable=True, comment='审批人'),
    sa.Column('send_user', sa.Integer(), nullable=True, comment='最后审批人'),
    sa.Column('message', sa.VARCHAR(length=100), nullable=True, comment='内容'),
    sa.Column('status', sa.Integer(), nullable=True, comment='1同意 2不同意 3回退'),
    sa.Column('audit_time', sa.DateTime(), nullable=True, comment='审核时间'),
    sa.PrimaryKeyConstraint('id'),
    comment='审批记录表'
    )
    op.create_index(op.f('ix_audit_id'), 'audit', ['id'], unique=False)
    op.create_table('classify',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('create_time', sa.DateTime(), server_default=sa.text('now()'), nullable=True, comment='创建时间'),
    sa.Column('update_time', sa.DateTime(), server_default=sa.text('now()'), nullable=True, comment='更新时间'),
    sa.Column('is_delete', sa.Integer(), server_default='0', nullable=True, comment='逻辑删除:0=未删除,1=删除'),
    sa.Column('cname', sa.VARCHAR(length=32), nullable=True, comment='名称'),
    sa.PrimaryKeyConstraint('id'),
    comment='分类表'
    )
    op.create_index(op.f('ix_classify_id'), 'classify', ['id'], unique=False)
    op.create_table('company',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('create_time', sa.DateTime(), server_default=sa.text('now()'), nullable=True, comment='创建时间'),
    sa.Column('update_time', sa.DateTime(), server_default=sa.text('now()'), nullable=True, comment='更新时间'),
    sa.Column('is_delete', sa.Integer(), server_default='0', nullable=True, comment='逻辑删除:0=未删除,1=删除'),
    sa.Column('cname', sa.VARCHAR(length=25), nullable=True, comment='公司名称'),
    sa.Column('ctype', sa.VARCHAR(length=25), nullable=True, comment='公司属性'),
    sa.Column('coding', sa.Integer(), nullable=True, comment='公司编码'),
    sa.Column('sort', sa.DateTime(), nullable=True, comment='公司排序'),
    sa.PrimaryKeyConstraint('id'),
    comment='公司表'
    )
    op.create_index(op.f('ix_company_id'), 'company', ['id'], unique=False)
    op.create_table('dutuies',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('create_time', sa.DateTime(), server_default=sa.text('now()'), nullable=True, comment='创建时间'),
    sa.Column('update_time', sa.DateTime(), server_default=sa.text('now()'), nullable=True, comment='更新时间'),
    sa.Column('is_delete', sa.Integer(), server_default='0', nullable=True, comment='逻辑删除:0=未删除,1=删除'),
    sa.Column('dname', sa.VARCHAR(length=25), nullable=True, comment='职务名称'),
    sa.Column('sort', sa.Integer(), nullable=True, comment='排序'),
    sa.PrimaryKeyConstraint('id'),
    comment='职务表'
    )
    op.create_index(op.f('ix_dutuies_id'), 'dutuies', ['id'], unique=False)
    op.create_table('resource',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('create_time', sa.DateTime(), server_default=sa.text('now()'), nullable=True, comment='创建时间'),
    sa.Column('update_time', sa.DateTime(), server_default=sa.text('now()'), nullable=True, comment='更新时间'),
    sa.Column('is_delete', sa.Integer(), server_default='0', nullable=True, comment='逻辑删除:0=未删除,1=删除'),
    sa.Column('rname', sa.VARCHAR(length=25), nullable=True, comment='名称'),
    sa.Column('pid', sa.Integer(), nullable=True, comment='0菜单  1资源'),
    sa.Column('url', sa.VARCHAR(length=300), nullable=True, comment='路由'),
    sa.Column('type', sa.Integer(), nullable=True, comment='1接口权限 2页面'),
    sa.PrimaryKeyConstraint('id'),
    comment='资源表'
    )
    op.create_index(op.f('ix_resource_id'), 'resource', ['id'], unique=False)
    op.create_table('role',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('create_time', sa.DateTime(), server_default=sa.text('now()'), nullable=True, comment='创建时间'),
    sa.Column('update_time', sa.DateTime(), server_default=sa.text('now()'), nullable=True, comment='更新时间'),
    sa.Column('is_delete', sa.Integer(), server_default='0', nullable=True, comment='逻辑删除:0=未删除,1=删除'),
    sa.Column('rname', sa.VARCHAR(length=25), nullable=True, comment='角色名称'),
    sa.PrimaryKeyConstraint('id'),
    comment='角色表'
    )
    op.create_index(op.f('ix_role_id'), 'role', ['id'], unique=False)
    op.create_table('role_resource',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('create_time', sa.DateTime(), server_default=sa.text('now()'), nullable=True, comment='创建时间'),
    sa.Column('update_time', sa.DateTime(), server_default=sa.text('now()'), nullable=True, comment='更新时间'),
    sa.Column('is_delete', sa.Integer(), server_default='0', nullable=True, comment='逻辑删除:0=未删除,1=删除'),
    sa.Column('role_id', sa.Integer(), nullable=True, comment='角色id'),
    sa.Column('resource_id', sa.Integer(), nullable=True, comment='资源id'),
    sa.PrimaryKeyConstraint('id'),
    comment='角色资源表'
    )
    op.create_index(op.f('ix_role_resource_id'), 'role_resource', ['id'], unique=False)
    op.create_table('station',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('create_time', sa.DateTime(), server_default=sa.text('now()'), nullable=True, comment='创建时间'),
    sa.Column('update_time', sa.DateTime(), server_default=sa.text('now()'), nullable=True, comment='更新时间'),
    sa.Column('is_delete', sa.Integer(), server_default='0', nullable=True, comment='逻辑删除:0=未删除,1=删除'),
    sa.Column('sname', sa.VARCHAR(length=25), nullable=True, comment='岗位名称'),
    sa.Column('sort', sa.Integer(), nullable=True, comment='排序'),
    sa.PrimaryKeyConstraint('id'),
    comment='岗位表'
    )
    op.create_index(op.f('ix_station_id'), 'station', ['id'], unique=False)
    op.create_table('task',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('create_time', sa.DateTime(), server_default=sa.text('now()'), nullable=True, comment='创建时间'),
    sa.Column('update_time', sa.DateTime(), server_default=sa.text('now()'), nullable=True, comment='更新时间'),
    sa.Column('is_delete', sa.Integer(), server_default='0', nullable=True, comment='逻辑删除:0=未删除,1=删除'),
    sa.Column('work_id', sa.Integer(), nullable=True, comment='工作流id'),
    sa.Column('user_id', sa.Integer(), nullable=True, comment='用户id'),
    sa.Column('message', sa.VARCHAR(length=100), nullable=True, comment='内容'),
    sa.Column('audit_list', sa.VARCHAR(length=200), nullable=True, comment='审批人列表'),
    sa.Column('next_audit', sa.VARCHAR(length=200), nullable=True, comment='下一审批人'),
    sa.Column('status', sa.Integer(), nullable=True, comment='1开始  2审批人中  3已完成'),
    sa.PrimaryKeyConstraint('id'),
    comment='任务表'
    )
    op.create_index(op.f('ix_task_id'), 'task', ['id'], unique=False)
    op.create_table('user',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('create_time', sa.DateTime(), server_default=sa.text('now()'), nullable=True, comment='创建时间'),
    sa.Column('update_time', sa.DateTime(), server_default=sa.text('now()'), nullable=True, comment='更新时间'),
    sa.Column('is_delete', sa.Integer(), server_default='0', nullable=True, comment='逻辑删除:0=未删除,1=删除'),
    sa.Column('uname', sa.VARCHAR(length=25), nullable=True, comment='姓名'),
    sa.Column('account', sa.VARCHAR(length=25), nullable=True, comment='登录账号'),
    sa.Column('number', sa.VARCHAR(length=30), nullable=True, comment='工号'),
    sa.Column('mobile', sa.VARCHAR(length=11), nullable=True, comment='手机号'),
    sa.Column('company_id', sa.Integer(), nullable=True, comment='所属公司'),
    sa.Column('station_id', sa.Integer(), nullable=True, comment='岗位id'),
    sa.Column('dutuies_id', sa.Integer(), nullable=True, comment='职务id'),
    sa.Column('role_id', sa.Integer(), nullable=True, comment='角色id'),
    sa.PrimaryKeyConstraint('id'),
    comment='用户表'
    )
    op.create_index(op.f('ix_user_id'), 'user', ['id'], unique=False)
    op.create_table('work',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('create_time', sa.DateTime(), server_default=sa.text('now()'), nullable=True, comment='创建时间'),
    sa.Column('update_time', sa.DateTime(), server_default=sa.text('now()'), nullable=True, comment='更新时间'),
    sa.Column('is_delete', sa.Integer(), server_default='0', nullable=True, comment='逻辑删除:0=未删除,1=删除'),
    sa.Column('cname', sa.VARCHAR(length=32), nullable=True, comment='名称'),
    sa.Column('reason', sa.VARCHAR(length=32), nullable=True, comment='理由'),
    sa.Column('status', sa.Integer(), nullable=True, comment='1没审核  2通过  3没通过'),
    sa.PrimaryKeyConstraint('id'),
    comment='工作流表'
    )
    op.create_index(op.f('ix_work_id'), 'work', ['id'], unique=False)
    op.create_table('work_right',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('create_time', sa.DateTime(), server_default=sa.text('now()'), nullable=True, comment='创建时间'),
    sa.Column('update_time', sa.DateTime(), server_default=sa.text('now()'), nullable=True, comment='更新时间'),
    sa.Column('is_delete', sa.Integer(), server_default='0', nullable=True, comment='逻辑删除:0=未删除,1=删除'),
    sa.Column('role_id', sa.Integer(), nullable=True, comment='角色id'),
    sa.Column('work_id', sa.Integer(), nullable=True, comment='工作流id'),
    sa.PrimaryKeyConstraint('id'),
    comment='工作流权限表'
    )
    op.create_index(op.f('ix_work_right_id'), 'work_right', ['id'], unique=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_work_right_id'), table_name='work_right')
    op.drop_table('work_right')
    op.drop_index(op.f('ix_work_id'), table_name='work')
    op.drop_table('work')
    op.drop_index(op.f('ix_user_id'), table_name='user')
    op.drop_table('user')
    op.drop_index(op.f('ix_task_id'), table_name='task')
    op.drop_table('task')
    op.drop_index(op.f('ix_station_id'), table_name='station')
    op.drop_table('station')
    op.drop_index(op.f('ix_role_resource_id'), table_name='role_resource')
    op.drop_table('role_resource')
    op.drop_index(op.f('ix_role_id'), table_name='role')
    op.drop_table('role')
    op.drop_index(op.f('ix_resource_id'), table_name='resource')
    op.drop_table('resource')
    op.drop_index(op.f('ix_dutuies_id'), table_name='dutuies')
    op.drop_table('dutuies')
    op.drop_index(op.f('ix_company_id'), table_name='company')
    op.drop_table('company')
    op.drop_index(op.f('ix_classify_id'), table_name='classify')
    op.drop_table('classify')
    op.drop_index(op.f('ix_audit_id'), table_name='audit')
    op.drop_table('audit')
    # ### end Alembic commands ###
