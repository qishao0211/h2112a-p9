# #!/usr/bin/env python
# # -*- coding: utf-8 -*-
# # @Time    : 2020/7/7 16:38
# # @Author  : CoderCharm
# # @File    : curd_user.py
# # @Software: PyCharm
# # @Desc    :
# """
#
# """
#
from typing import Optional
import re, json
from api.utils.myredis import myredis
from sqlalchemy import func
from sqlalchemy.orm import Session
from api.utils.myjwt import myjwt
from core.security import get_password_hash, verify_password
from api.common.curd_base import CRUDBase
from api.models.auth import Project, ApiSet, ApiMsg
from ..schemas import project_schema


class CRUDUser(CRUDBase):

    @staticmethod
    def create(db: Session, obj_info: project_schema.ProjectCreate) -> Project:
        db_obj = Project(
            name=obj_info.name,
            environment_list=obj_info.environment_list,
            user_name=obj_info.user_name,
            user_id=obj_info.user_id
        )
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    @staticmethod
    def create_api_msg(db: Session, obj_info: project_schema.ApiMsgCreate) -> Project:
        db_obj = ApiMsg(
            project_id=obj_info.project_id,
            apiset_id=obj_info.apiset_id,
            url=obj_info.url,
            method=obj_info.method,
            name=obj_info.name,
            header=obj_info.header,
            variable=obj_info.variable,
            validate=obj_info.validates,
        )
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    @staticmethod
    def value_api_msg(obj_info: project_schema.ApiMsgCreate):
        if not all([
            obj_info.project_id,
            obj_info.apiset_id,
            obj_info.url,
            obj_info.name,
            obj_info.method,
            obj_info.header,
            obj_info.variable,
            obj_info.validates,
        ]):
            return False
        return True

    @staticmethod
    def query_all(db: Session, page: int = 1, page_size: int = 10) -> dict:
        """
        查询数据列表
        :param db:
        :param page:
        :param page_size:
        :return:
        """
        temp_page = (page - 1) * page_size
        # 查询数量
        total = db.query(func.count(Project.id)).scalar()
        # 查询结果集
        query_obj = db.query(Project).filter(Project.is_delete == 0).offset(
            temp_page).limit(page_size).all()

        items = [{"id": obj.id, "name": obj.name} for obj in query_obj]
        return {
            "items": items,
            "total": total,
        }

    @staticmethod
    def query_user_all(db: Session, user_id: int) -> dict:
        # 查询结果集
        query_obj = db.query(Project).filter(Project.user_id == user_id).all()

        items = [{"id": obj.id, "name": obj.name, 'user_name': obj.user_name}
                 for obj in query_obj]
        return {
            "items": items,
        }

    @staticmethod
    def api_msg_info(db: Session, id: int) -> dict:
        # 查询结果集
        query_obj = db.query(ApiMsg).filter(ApiMsg.id == id).first()
        project_info = db.query(Project).filter(Project.id == query_obj.project_id).first()
        set_info = db.query(ApiSet).filter(ApiSet.id == query_obj.apiset_id).first()
        url = json.loads(project_info.environment_list)[0]['url'] + set_info.num + query_obj.url
        # 格式化传递的参数
        data = {}
        for i in json.loads(query_obj.variable):
            data[i['key']] = i['value']

        items = {
            'project_id': query_obj.project_id,
            'apiset_id': query_obj.apiset_id,
            'url': url,
            'method': query_obj.method,
            'name': query_obj.name,
            'header': query_obj.header,
            'variable': data,
            'validate': json.loads(query_obj.validate),
        }
        return items


curd_project = CRUDUser(Project)
