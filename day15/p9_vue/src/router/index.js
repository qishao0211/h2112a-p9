import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/components/Index'
import Login from '@/components/Login.vue'
import MyTest from '@/components/MyTest.vue'

Vue.use(Router)

export default new Router({
    routes: [{
        path: '/',
        name: 'Index',
        component: Index,
        children: [{
                path: '/page1',
                name: 'page1',
                component: function() {
                    return import ('../components/Page1.vue')
                },

            },
            {
                path: '/page2',
                name: 'page2',
                component: function() {
                    return import ('../components/Page2.vue')
                }
            },
            {
                path: '/project',
                name: 'Project',
                component: function() {
                    return import ('../components/Project/Project.vue')
                }
            },
            {
                path: '/test',
                name: 'test',
                component: function() {
                    return import ('../components/test/MyTest.vue')
                }
            },
            {
                path: '/case',
                name: 'Case',
                component: function() {
                    return import ('../components/case/Case.vue')
                }
            },
        ]
    },
    {
        path: '/login',
        name: 'Login',
        component: Login
    },
    {
        path: '/test2',
        name: 'MyTest',
        component: MyTest
    },
]
    
})