// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

Vue.config.productionTip = false
import axios from 'axios'
Vue.prototype.axios = axios;
axios.defaults.baseURL = "http://127.0.0.1:8010/api/mall/v1"


import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

Vue.use(ElementUI);

// import echarts from 'echarts'
// Vue.prototype.$echarts = echarts

// 白名单过滤
router.beforeEach((to, from, next) =>{
    var white = ['/reg', '/login']
    if (white.indexOf(to.path) >= 0){
        next()
    } else {
        var token = localStorage.getItem('token')
        if (token){
            next()
        } else {
            router.push('/login')
        }
    }
})

// 拦截器请求前
axios.interceptors.request.use(config=>{
    console.log('请求前的拦截器：', config)
    var token = localStorage.getItem('token')
    if (token){
        config.headers['token'] = token
    }
    return config
})


/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    components: { App },
    template: '<App/>'
})