import pymysql

con = pymysql.connect(host="localhost", port=3306, user="root", password="shayebushi", database="data1", charset="utf8")

c1 = con.cursor()
sql="create table staas(" \
    "sid int primary key auto_increment," \
    "sname varchar(20) not null," \
    "score float(3,1) default 0," \
    "joindate date not null " \
    ")"
res=c1.execute(sql)

## 创建老师表 字段  id主键自增  name老师名字
# create table teacher(id int primary key auto_increment,name varchar(100));
##创建科目表  id主键自增  name科目名字  tid老师id
# create table course(id int primary key auto_increment,name varchar(100),tid int);
##创建学生表  id主键自增  name学生名字
# create table stu(id int primary key auto_increment,name varchar(100));
##创建分数表  id主键自增  name科目名字  sid学生id cid科目id score分数
# create table score(id int primary key auto_increment,sid int,cid int,score int);

##老师表数据
#  insert into teacher values(0,'王老师'),(0,'李老师');
##科目表数据
#  insert into course values(0,'java',1),(0,'python',2);
## 学生表数据
#  insert into stu values(0,'小明'),(0,'小李'),(0,'小张');
## 分数表数据
#  insert into score values(0,1,1,50),(0,1,2,100),(0,2,1,60),(0,3,2,70);

# 1，查询平均成绩大于60分的同学的学号和平均成绩
# avg()求出平津成绩  group by 根据某个字段进行分组  having 查询结果后进行过滤
# select id,avg(score)  as sc from score group by sid having sc>60
# 2，查询所有同学的学号，姓名，选课数，总成绩
# 方法一
# select stu.id,stu.name,count(score.id),sum(score.score) from stu inner join score on stu.id =score.sid group by stu.id,stu.name
# 方法二
# select stu.name,b.* from stu inner join (select sid,count(id),sum(score) from score group by sid) as b on stu.id=b.sid;

# 3，查询性'李'的老师个数
# select name,count(*) from teacher where name like '李%' group by name;

# 4，查询'python'课程比'java'课程成绩高的所有学生的学号
select score.score,score.sid from score inner join course on score.cid=course.id where course.name='python';
select score.score,score.sid from score inner join course on score.cid=course.id where course.name='java';
select stu.name,a.score as python,b.score as java from (select score.score,score.sid from score inner join course on score.cid=course.id where course.name='python')
as a inner join (select score.score,score.sid from score inner join course on score.cid=course.id where course.name='java')
as b on a.sid=b.sid inner join stu on a.sid=stu.id where a.score>b.score;



# 5，查询没有学过王老师课的同学的学号，姓名
# 分数表查询上过王老师课的所有学生id   通过内连接查询出分数表中王老师教过的课程
select sid from score where cid in(select course.id from course inner join
teacher on course.tid=teacher.id where teacher.name='王老师')
查询学生表 看那个学生id不在王老师教过的学生id中
select * from stu where id not in(select sid from score where cid in(select course.id from course inner join
teacher on course.tid=teacher.id where teacher.name='王老师'));









# select id,avg(score)  as sc from score group by sid having sc>70

# select name,count(*) from teacher where name like '李%';
# select name,count(*) from teacher where name like '李%' group by name;