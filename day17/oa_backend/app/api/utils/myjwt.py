import jwt
import time

class MyJwt():
    def jwt_token(self, payload):
        payload['exp'] = int(time.time()) + 3600 * 24 * 15
        token = jwt.encode(payload, '1', algorithm='HS256')
        return token

    def jwt_payload(self, token):
        try:
            payload = jwt.decode(token, '1', algorithms='HS256')
            return payload
        except Exception as e:
            print(e)
            return False


myjwt = MyJwt()
# token = myjwt.jwt_token({'name': 'zhangsan', 'age': 18})
# payload = myjwt.jwt_payload(token)
# print(payload)