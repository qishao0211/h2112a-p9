#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2020/7/7 14:11
# @Author  : CoderCharm
# @File    : views.py
# @Software: PyCharm
# @Desc    :
"""

"""
import requests

from fastapi import APIRouter, Depends
from api.common import deps
from api.utils import response_code
from .schemas import project_schema

from .crud import curd_project

router = APIRouter()


# http://127.0.0.1:8010/api/mall/v1/admin/test/add/case
# 添加项目的基本信息
@router.post('/add/case', summary='基本信息')
async def add_basics(
        obj_info: project_schema.ApiMsgCreate,
        db: str = Depends(deps.get_db)
):
    print('>>>>', obj_info)
    # 判断数据是否为空
    if not curd_project.value_api_msg(obj_info):
        return response_code.resp_400(message='参数不能为空')

    curd_project.create_api_msg(db, obj_info)
    return response_code.resp_200(message='添加成功')


# 展示项目信息《分页》
@router.get('/get/case', summary='展示基本信息')
async def get_project(
        db: str = Depends(deps.get_db),
        page: int = 1,
        page_size: int = 5
):
    data = curd_project.query_all(db, page, page_size)
    return response_code.resp_200(data=data)

