# #!/usr/bin/env python
# # -*- coding: utf-8 -*-
# # @Time    : 2020/7/7 16:38
# # @Author  : CoderCharm
# # @File    : curd_user.py
# # @Software: PyCharm
# # @Desc    :
# """
#
# """
#
from typing import Optional
import re, json
from api.utils.myredis import myredis
from sqlalchemy import func
from sqlalchemy.orm import Session
from api.utils.myjwt import myjwt
from core.security import get_password_hash, verify_password
from api.common.curd_base import CRUDBase
from api.models.auth import CaseSet
from ..schemas import project_schema


class CRUDUser(CRUDBase):

    @staticmethod
    def create_api_msg(db: Session, obj_info: project_schema.ApiMsgCreate) -> CaseSet:
        db_obj = CaseSet(
            project_id=obj_info.project_id,
            name=obj_info.name,
        )
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    @staticmethod
    def value_api_msg(obj_info: project_schema.ApiMsgCreate):
        if not all([
            obj_info.project_id,
            obj_info.name,
        ]):
            return False
        return True

    @staticmethod
    def query_all(db: Session, page: int = 1, page_size: int = 10) -> dict:
        """
        查询数据列表
        :param db:
        :param page:
        :param page_size:
        :return:
        """
        temp_page = (page - 1) * page_size
        # 查询数量
        total = db.query(func.count(CaseSet.id)).scalar()
        # 查询结果集
        query_obj = db.query(CaseSet).filter(CaseSet.is_delete == 0).offset(
            temp_page).limit(page_size).all()

        items = [{"id": obj.id, "name": obj.name} for obj in query_obj]
        return {
            "items": items,
            "total": total,
        }

    @staticmethod
    def query_user_all(db: Session, user_id: int) -> dict:
        # 查询结果集
        query_obj = db.query(Project).filter(Project.user_id == user_id).all()

        items = [{"id": obj.id, "name": obj.name, 'user_name': obj.user_name}
                 for obj in query_obj]
        return {
            "items": items,
        }



curd_project = CRUDUser(CaseSet)
