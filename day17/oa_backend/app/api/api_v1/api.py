from fastapi import APIRouter

from api.api_v1 import auth, project, test, case


api_v1_router = APIRouter()
api_v1_router.include_router(auth.router, prefix="/admin/auth", tags=["用户"])
api_v1_router.include_router(project.router, prefix="/admin/project", tags=["项目"])
api_v1_router.include_router(test.router, prefix="/admin/test", tags=["测试"])
api_v1_router.include_router(case.router, prefix="/admin/case", tags=["用例"])
