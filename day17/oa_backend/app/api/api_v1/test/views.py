#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2020/7/7 14:11
# @Author  : CoderCharm
# @File    : views.py
# @Software: PyCharm
# @Desc    :
"""

"""
import requests

from fastapi import APIRouter, Depends
from api.common import deps
from api.utils import response_code
from .schemas import project_schema

from .crud import curd_project

router = APIRouter()


# http://127.0.0.1:8010/api/mall/v1/admin/test/add/test
# 添加项目的基本信息
@router.get('/add/test', summary='基本信息')
async def add_basics(
        id: int = 0,
        db: str = Depends(deps.get_db)
):
    # 根据id获取数据
    res = curd_project.api_msg_info(db, id)
    newlist = ['GET', 'DELETE']

    if res['method'] in newlist:
        headers = ''
        if res['header']:
            headers = res['header']
        result = requests.get(res['url'], params=res['variable'], headers=headers)
        print('>>>', result.json())
        return result
    else:
        result = requests.post(res['url'], json=res['variable'], headers=None)
        print(result.json())

    # if res['validate']['code'] == result.json()['code']:


    return response_code.resp_200(message='添加成功')


# 展示项目信息《分页》
@router.get('/get/project', summary='展示基本信息')
async def get_project(
        db: str = Depends(deps.get_db),
        page: int = 1,
        page_size: int = 5
):
    data = curd_project.query_all(db, page, page_size)
    return response_code.resp_200(data=data)

