#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2020/7/11 11:00
# @Author  : CoderCharm
# @File    : rbacm.py
# @Software: PyCharm
# @Desc    :
"""
角色表crud操作
"""

from typing import Optional
from sqlalchemy.orm import Session

from api.common.curd_base import CRUDBase
from api.models.auth import AdminRole, Role
from ..schemas import role_schema
# from api.api_v1.rbacm.schemas import role_schema


class CRUDRole(CRUDBase[AdminRole, role_schema.RoleCreate, role_schema.RoleCreate]):

    @staticmethod
    def query_role(db: Session, *, role_id: int) -> Optional[AdminRole]:
        """
        此role_id是否存在
        :param db:
        :param role_id:
        :return:
        """
        return db.query(AdminRole).filter(AdminRole.role_id == role_id).first()

    def create(self, db: Session, *, obj_in: role_schema.RoleCreate):
        db_obj = Role(
            rname=obj_in.name
        )
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj


curd_role = CRUDRole(Role)
