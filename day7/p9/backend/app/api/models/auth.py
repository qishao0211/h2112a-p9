#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2020/7/7 14:16
# @Author  : CoderCharm
# @File    : auth.py
# @Software: PyCharm
# @Desc    :
"""

用户模块

"""
from datetime import datetime

from sqlalchemy import Boolean, Column, Integer, String, VARCHAR, BIGINT, SmallInteger, DateTime
from api.db.base_class import Base, gen_uuid


class AdminUser(Base):
    """
    管理员表
    """
    __tablename__ = "admin_user"
    user_id = Column(VARCHAR(32), default=gen_uuid, unique=True, comment="用户id")
    email = Column(VARCHAR(128), unique=True, index=True, nullable=False, comment="邮箱")
    phone = Column(VARCHAR(16), unique=True, index=True, nullable=True, comment="手机号")
    nickname = Column(VARCHAR(128), comment="管理员昵称")
    avatar = Column(VARCHAR(256), comment="管理员头像")
    hashed_password = Column(VARCHAR(128), nullable=False, comment="密码")
    is_active = Column(Integer, default=False, comment="邮箱是否激活 0=未激活 1=激活", server_default="0")
    role_id = Column(Integer, comment="角色表")
    __table_args__ = ({'comment': '管理员表'})


class AdminRole(Base):
    """
    简单的用户角色表设计
    """
    __tablename__ = "admin_role"
    role_id = Column(Integer, primary_key=True, index=True, comment="角色Id")
    role_name = Column(VARCHAR(64), comment="角色名字")
    permission_id = Column(BIGINT, comment="权限ID")
    re_mark = Column(VARCHAR(128), comment="备注信息")
    __table_args__ = ({'comment': '管理员角色'})


class MallUser(Base):
    """
    用户表
    """
    __tablename__ = "mall_user"
    user_id = Column(VARCHAR(32), default=gen_uuid, index=True, unique=True, comment="用户id")
    nickname = Column(VARCHAR(128), comment="用户昵称(显示用可更改)")
    username = Column(VARCHAR(128), comment="用户名(不可更改)")
    avatar = Column(VARCHAR(256), nullable=True, comment="用户头像")
    hashed_password = Column(VARCHAR(128), nullable=False, comment="密码")
    phone = Column(VARCHAR(16), unique=True, index=True, nullable=True, comment="手机号")
    gender = Column(SmallInteger, default=0, comment="性别 0=未知 1=男 2=女", server_default="0")
    register_time = Column(DateTime, default=datetime.now, comment="注册事件")
    last_login_time = Column(DateTime, default=datetime.now, comment="上次登录时间")
    last_login_ip = Column(VARCHAR(64), nullable=True, comment="上次登录IP")
    register_ip = Column(VARCHAR(64), nullable=True, comment="注册IP")
    weixin_openid = Column(VARCHAR(64), nullable=True, comment="微信openId")
    country = Column(VARCHAR(64), nullable=True, comment="国家")
    province = Column(VARCHAR(64), nullable=True, comment="省")
    city = Column(VARCHAR(64), nullable=True, comment="市")
    __table_args__ = ({'comment': '用户表'})


class MallAddress(Base):
    """
    用户地址列表
    """
    name = Column(VARCHAR(64), comment="用户昵称")
    user_id = Column(VARCHAR(32), comment="用户id")
    country_id = Column(Integer, comment="国家Id")
    province_id = Column(Integer, comment="省id")
    city_id = Column(Integer, comment="市id")
    district_id = Column(Integer, comment="区id")
    address = Column(VARCHAR(128), comment="详细地址")
    phone = Column(VARCHAR(64), comment="手机号")
    is_default = Column(SmallInteger, default=0, comment="是否默认地址", server_default="0")
    __table_args__ = ({'comment': '地址表'})


class MallSearchHistory(Base):
    """
    搜索记录
    """
    keyword = Column(VARCHAR(64), comment="搜索关键词")
    search_origin = Column(SmallInteger, default=1, comment="搜索来源 1=小程序 2=APP 3=PC", server_default="1")
    user_id = Column(VARCHAR(32), index=True, comment="用户id")
    __table_args__ = ({'comment': '搜索记录'})


class MallSiteNotice(Base):
    """
    站点消息
    """
    enabled = Column(SmallInteger, default=1, comment="是否开启 0=为开启 1=开启", server_default="1")
    content = Column(VARCHAR(256), comment="全局消息通知")
    start_time = Column(DateTime, comment="开始时间")
    end_time = Column(DateTime, comment="结束时间")

    __table_args__ = ({'comment': '站点消息'})


"""
=============================================
"""


class Company(Base):
    """
    公司表
    """
    cname = Column(VARCHAR(25), comment="公司名称")
    ctype = Column(Integer, comment="公司类型,0：组织，1：部门")
    coding = Column(VARCHAR(25), comment="公司编码")
    sort = Column(Integer, comment="公司排序")
    pid = Column(Integer, comment='公司id')

    __table_args__ = ({'comment': '公司表'})


class User(Base):
    """
    用户表
    """
    uname = Column(VARCHAR(25), comment="姓名")
    account = Column(VARCHAR(25), comment="登录账号")
    number = Column(VARCHAR(30), comment="工号")
    mobile = Column(VARCHAR(11), comment="手机号")
    company_id = Column(Integer, comment="所属公司")
    station_id = Column(Integer, comment="岗位id")
    dutuies_id = Column(Integer, comment="职务id")
    role_id = Column(Integer, comment="角色id")

    __table_args__ = ({'comment': '用户表'})


class Dutuies(Base):
    """
    职务表
    """
    dname = Column(VARCHAR(25), comment="职务名称")
    sort = Column(Integer, comment="排序")

    __table_args__ = ({'comment': '职务表'})


class Station(Base):
    """
    岗位表
    """
    sname = Column(VARCHAR(25), comment="岗位名称")
    sort = Column(Integer, comment="排序")

    __table_args__ = ({'comment': '岗位表'})


class Role(Base):
    """
    角色表
    """
    rname = Column(VARCHAR(25), comment="角色名称")
    pid = Column(Integer, comment="分类（自关联）")
    roletype = Column(Integer, comment='类型（1用户角色， 2继承角色）', default=1)
    promition = Column(Integer, comment='二进制运算')

    __table_args__ = ({'comment': '角色表'})


class Resource(Base):
    """
     资源表
    """
    rname = Column(VARCHAR(25), comment="名称")
    pid = Column(Integer, comment="0菜单  1资源")
    url = Column(VARCHAR(300), comment="路由")
    type = Column(Integer, comment="1接口权限 2页面")
    promition=Column(Integer,comment='二进制运算')

    __table_args__ = ({'comment': '资源表'})


class RoleResource(Base):
    """
    角色资源表
    """
    role_id = Column(Integer, comment="角色id")
    resource_id = Column(Integer, comment="资源id")

    __table_args__ = ({'comment': '角色资源表'})


# 应用工作流

class Classify(Base):
    """
    分类表
    """
    cname = Column(VARCHAR(32), comment="名称")

    __table_args__ = ({'comment': '分类表'})


class Work(Base):
    """
    工作流表
    """
    cname = Column(VARCHAR(32), comment="名称")
    reason = Column(VARCHAR(32), comment="理由")
    status = Column(Integer, comment="1没审核  2通过  3没通过")

    __table_args__ = ({'comment': '工作流表'})


class WorkRight(Base):
    """
    工作流权限表
    """
    role_id = Column(Integer, comment="角色id")
    work_id = Column(Integer, comment="工作流id")

    __table_args__ = ({'comment': '工作流权限表'})


class Task(Base):
    """
    任务表
    """
    work_id = Column(Integer, comment="工作流id")
    user_id = Column(Integer, comment="用户id")
    message = Column(VARCHAR(100), comment="内容")
    audit_list = Column(VARCHAR(200), comment="审批人列表")
    next_audit = Column(VARCHAR(200), comment="下一审批人")
    status = Column(Integer, comment="1开始  2审批人中  3已完成")

    __table_args__ = ({'comment': '任务表'})


class Audit(Base):
    """
    审批记录表
    """
    work_id = Column(Integer, comment="工作流id")
    task_id = Column(Integer, comment="任务id")
    audit_user = Column(Integer, comment="审批人")
    send_user = Column(Integer, comment="最后审批人")
    message = Column(VARCHAR(100), comment="内容")
    status = Column(Integer, comment="1同意 2不同意 3回退")
    audit_time = Column(DateTime, comment='审核时间')

    __table_args__ = ({'comment': '审批记录表'})


class Mutual(Base):
    """
    角色配置资源的互斥表
    """
    resource_id1 = Column(Integer, comment="一级资源id")
    resource_id2 = Column(Integer, comment="二级资源id")

    __table_args__ = ({'comment': '角色配置资源的互斥表'})

