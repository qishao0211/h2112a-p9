#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2020/7/13 13:37
# @Author  : CoderCharm
# @File    : views.py
# @Software: PyCharm
# @Github  : github/CoderCharm
# @Email   : wg_python@163.com
# @Desc    :
"""

"""
from typing import Union, Any
from fastapi import APIRouter, Depends, Query
from sqlalchemy.orm import Session

from api.common import deps
from api.common.logger import logger
from api.utils import response_code
# from app.api.api_v1.auth.crud.role import curd_role

from .schemas import goods_schema, role_schema
from .crud.category import curd_category

router = APIRouter()




@router.post("/add/company", summary="添加公司")
async def add_role(
        role_info: role_schema.RoleCreate,
        db: Session = Depends(deps.get_db),
):
    print('>>>', role_info)
    # logger.info(f"添加分类->用户id:，分类名:{role_info.name}")
    curd_category.create(db=db, obj_in=role_info)
    return response_code.resp_200(message="公司添加成功")


@router.get("/get/company", summary="查询公司列表")
async def get_role(
        db: Session = Depends(deps.get_db)
):
    # logger.info(f"查询分类列表->用户id:,当前页{page}长度{page_size}")
    response_result = curd_category.company_all(db)
    return response_code.resp_200(data=response_result)



