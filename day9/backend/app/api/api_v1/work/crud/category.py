#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2020/7/25 09:02
# @Author  : CoderCharm
# @File    : category.py
# @Software: PyCharm
# @Github  : github/CoderCharm
# @Email   : wg_python@163.com
# @Desc    :
"""

"""

from pydantic import conint
from sqlalchemy import func, or_
from sqlalchemy.orm import Session

from api.common.curd_base import CRUDBase
from api.models.goods import MallCategory
from api.models.auth import Work, WorkAudit
from ..schemas import category_schema
import json


def work_audit_list(list2):
    # '[{"code":3,"name":"总监","id":19},{"code":2,"name":"副总经理","id":18}]'
    list_json = json.loads(list2)
    code_list = sorted(list_json, key=lambda x:x['code'], reverse=False)
    id_list = [i['id'] for i in code_list]
    return json.dumps(id_list)



class CRUDCategory(CRUDBase[MallCategory, category_schema.CategoryCreate, category_schema.CategoryUpdate]):

    def query_obj(self, db: Session, *, cate_id: int) -> dict:
        """
        查询单条数据
        :param db:
        :param cate_id:
        :return:
        """
        obj = self.get(db=db, id=cate_id)
        if not obj:
            return {}
        return {"id": obj.id, 'create_time': obj.create_time.strftime('%Y-%m-%d %H:%M:%S'), "name": obj.name,
                "front_desc": obj.front_desc, "parent_id": obj.parent_id, "sort_order": obj.sort_order,
                "icon_url": obj.icon_url, "enabled": obj.enabled}

    @staticmethod
    def search_field(db: Session, *, cate_info: category_schema.CategorySearch):
        temp_page = (cate_info.page - 1) * cate_info.page_size
        # 查询数量包含关键词的数量
        total = db.query(func.count(MallCategory.id)).filter(
            or_(MallCategory.name.contains(cate_info.key_world),
                MallCategory.front_desc.contains(cate_info.key_world))).scalar()
        # 查询name和front_desc包含搜索关键词的数据并分页
        search_obj = db.query(MallCategory).filter(
            or_(MallCategory.name.contains(cate_info.key_world),
                MallCategory.front_desc.contains(cate_info.key_world))).offset(
            temp_page).limit(cate_info.page_size).all()

        items = [{"id": obj.id, 'create_time': obj.create_time.strftime('%Y-%m-%d %H:%M:%S'), "name": obj.name,
                  "front_desc": obj.front_desc, "sort_order": obj.sort_order,
                  "icon_url": obj.icon_url, "enabled": obj.enabled} for obj in search_obj]
        return {
            "items": items,
            "total": total
        }

    @staticmethod
    def query_all(db: Session) -> dict:
        # 查询结果集
        total = db.query(Work).all()
        data = [{'name':i.cname, 'id': i.id} for i in total]
        return {
            "list": data
        }

    def create(self, db: Session, *, obj_in: category_schema.CategoryWork) -> Work:
        db_obj = Work(
            cname=obj_in.cname,
            reason=obj_in.reason,
            parmars=obj_in.parmars,
            status=obj_in.status,
        )
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    @staticmethod
    def update_work(db: Session, obj_in: category_schema.CategoryWorkUpd):
        print('>>>>', obj_in)
        db.query(Work).filter(Work.id == obj_in.work_id).update({
            Work.parmars: obj_in.parmars
        })
        db.commit()

    @staticmethod
    def update_enabled(db: Session, *, id: int, enabled: int):
        db.query(MallCategory).filter(MallCategory.id == id).update({MallCategory.enabled: enabled})
        db.commit()

    @staticmethod
    def work_audit(db: Session, obj_in: category_schema.CategoryWorkAudit):
        # 序列化数据
        list2 = work_audit_list(obj_in.id_list)
        print(">>>", list2)
        db_obj = WorkAudit(
            min=obj_in.min,
            max=obj_in.max,
            work_id=obj_in.work_id,
            id_list=list2,
        )
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj


curd_category = CRUDCategory(MallCategory)
