#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2020/7/7 14:11
# @Author  : CoderCharm
# @File    : views.py
# @Software: PyCharm
# @Desc    :
"""

"""
import random
from datetime import timedelta
from typing import Any, Union

from sqlalchemy.orm import Session
from fastapi import APIRouter, Depends
from core import security
from api.utils.myredis import myredis
from api.common import deps
from api.utils import response_code
from api.common.logger import logger

# from api.models import auth
from core.config import settings
from django.http import HttpResponse
from .schemas import user_schema, token_schema

from captcha.image import ImageCaptcha


from .crud import curd_user

router = APIRouter()

#
# @router.post("/login/access-token", summary="用户登录认证", response_model=token_schema.RespToken)
# async def login_access_token(
# ) -> Any:
#     """
#     用户登录
#     :param db:
#     :param user_info:
#     :return:
#     """
#
#     # 验证用户
#     user = curd_user.authenticate(db, email=user_info.username, password=user_info.password)
#     if not user:
#         logger.info(f"用户邮箱认证错误: email{user_info.username} password:{user_info.password}")
#         return response_code.resp_500(message="用户名或者密码错误")
#     elif not curd_user.is_active(user):
#         return response_code.resp_500(message="用户邮箱未激活")
#
#     access_token_expires = timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)
#
#     # 登录token 只存放了user.id
#     return response_code.resp_200(data={
#         "token": security.create_access_token(user.id, expires_delta=access_token_expires),
#     })
#
#
# @router.get("/user/info", summary="获取用户信息", response_model=user_schema.RespUserInfo)
# async def get_user_info(
# ) -> Any:
#     """
#     获取用户信息
#     :param db:
#     :param current_user:
#     :return:
#     """
#     role_info = curd_role.query_role(db, role_id=current_user.role_id)
#
#     return response_code.resp_200(data={
#         "role_id": current_user.role_id,
#         "role": role_info.role_name,
#         "nickname": current_user.nickname,
#         "avatar": current_user.avatar
#     })
#
#
# @router.post("/user/logout", summary="用户退出", response_model=user_schema.RespBase)
# async def user_logout():
#     """
#     用户退出
#     :param token_data:
#     :return:
#     """
#     logger.info(f"用户退出->用户id:{token_data.sub}")
#     return response_code.resp_200(message="logout success")
#
#
# # http://127.0.0.1:8010/api/mall/v1/admin/auth/test
#
# @router.get("/test", summary="测试")
# async def test():
#     print('================')
#     data = 'ヾ(๑╹◡╹)ﾉ"'
#     return data


# http://127.0.0.1:8010/api/mall/v1/admin/auth/login
# 登录
@router.get('/login', summary='用户登录')
async def login(
        db: Session = Depends(deps.get_db),
        mobile: str = '', code: str = ''):
    # 判断是否为空
    if not all([mobile, code]):
        return response_code.resp_1001(message='参数不能为空')

    # 判断手机号是否正确
    if not curd_user.mobile_auth(mobile):
        return response_code.resp_1002(message='手机号不正确')

    # 判断手机号是否存在
    if not curd_user.mobile_info(db, mobile):
        return response_code.resp_1003(message='用户不存在')

    # 判断验证码
    res = curd_user.mobile_code(mobile)
    if not res or res != code:
        return response_code.resp_1004(message='验证码已过期或不正确！')

    # 判断图形验证码
    # 生成token
    token = curd_user.user_token(db, mobile)

    return response_code.resp_200(message='登录成功', data=token)


# 发送验证码
@router.get('/code', summary='发送验证码')
async def code(mobile: str = ''):
    # 判断是否为空
    if not mobile:
        return response_code.resp_1001(message='请输入手机号')

    # 判断手机号是否正确
    res = curd_user.mobile_auth(mobile)
    if not res:
        return response_code.resp_1002(message='手机号不正确')

    # 查看验证码是否过期
    res = myredis.get_str(mobile)
    if res:
        return response_code.resp_200(message='一分钟只能发送一次')

    # 生成验证码
    code = random.randint(0,9)

    # 存入redis中
    myredis.set_str(mobile, code, 60)

    return response_code.resp_200(data=code, message='发送成功请注意查收')



# 生成图形验证码
# @router.get('/img', summary='生成图形验证码')
# async def image(uuid: str = ''):
#     # 随机生成一组数字
#     sms_code = str(random.randint(1000, 9999))
#
#     # 将数字生成图片
#     image = ImageCaptcha()
#     image_png = image.generate(sms_code)
#
#     # 添加redis
#     # myredis.set_str(uuid, text, 300)
#     return HttpResponse(image_png, content_type='image/png')
