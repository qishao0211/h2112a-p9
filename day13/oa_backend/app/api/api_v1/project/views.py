#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2020/7/7 14:11
# @Author  : CoderCharm
# @File    : views.py
# @Software: PyCharm
# @Desc    :
"""

"""
import random

from fastapi import APIRouter, Depends
from api.common import deps
from api.utils import response_code
from .schemas import project_schema

from .crud import curd_project

router = APIRouter()


# http://127.0.0.1:8010/api/mall/v1/admin/project/add/basics
# 添加项目的基本信息
@router.post('/add/basics', summary='基本信息')
async def add_basics(
        obj_info: project_schema.ProjectCreate,
        db: str = Depends(deps.get_db)
):
    print('>>>>', obj_info)
    curd_project.create(db, obj_info)
    return response_code.resp_200(message='添加成功')


# 展示项目信息《分页》
@router.get('/get/project', summary='展示基本信息')
async def get_project(
        db: str = Depends(deps.get_db),
        page: int = 1,
        page_size: int = 5
):
    data = curd_project.query_all(db, page, page_size)
    return response_code.resp_200(data=data)
