from fastapi import APIRouter

from api.api_v1 import auth, project


api_v1_router = APIRouter()
api_v1_router.include_router(auth.router, prefix="/admin/auth", tags=["用户"])
api_v1_router.include_router(project.router, prefix="/admin/project", tags=["项目"])
